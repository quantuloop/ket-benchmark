from ket_benchmark import *
from ket import kbw
if __name__ == '__main__':
    execute('KBW Dense', kbw.use_dense, 'data/kbw_dense.json', {
        "Hadamard": dict(func=hadamard, begin=3, end=27),
        "GHZ State": dict(func=ghz, begin=3, end=27),
        "W State": dict(func=w, begin=3, end=27),
        "Grover's Algorithm": dict(func=grover, begin=10, end=19),
        "Shor's Algorithm": dict(func=shor, range=range(12, 22, 2)),
        "Phase Estimation Algorithm": dict(func=phase_estimator, begin=10, end=26),
        "Random Quantum Circuit": dict(func=rqc, begin=3, end=21)
    })
    execute('KBW Sparse', kbw.use_sparse, 'data/kbw_sparse.json', setup={
        "Hadamard": dict(func=hadamard, begin=3, end=25),
        "GHZ State": dict(func=ghz, begin=3, end=25),
        "W State": dict(func=w, begin=3, end=25),
        "Grover's Algorithm": dict(func=grover, begin=3, end=15),
        "Shor's Algorithm": dict(func=shor, range=range(12, 28, 2)),
        "Phase Estimation Algorithm": dict(func=phase_estimator, begin=10, end=19),
        "Random Quantum Circuit": dict(func=rqc, begin=3, end=17)
    })
