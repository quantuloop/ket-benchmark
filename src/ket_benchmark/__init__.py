from __future__ import annotations
from itertools import combinations
from time import time
from shor import shor as _shor, _quantum_subroutine, list_N, list_x
from random import choice, seed, shuffle
from math import sqrt, pi
from ket import *
from ket import lib
from tqdm import tqdm
import json


def hadamard(n: int) -> float:
    q = quant(n)
    X(q)
    H(q)
    Z(q)
    H(q)
    assert (measure(q).value == 0)
    return quantum_exec_time()


def ghz(n: int) -> float:
    result = measure(lib.ghz(n)).value
    assert (result == 0 or result == 2**n-1)
    return quantum_exec_time()


def w(n: int) -> float:
    assert (bin(measure(lib.w(n)).value).count("1") == 1)
    return quantum_exec_time()


def grover(n: int) -> float:
    w = 3
    s = H(quant(n))
    steps = int((pi/4)*sqrt(2**n))
    for _ in range(steps):
        phase_on(w, s)
        with around(H, s):
            phase_on(0, s)
    state = dump(s).get_quantum_state()
    assert (max(state.items(), key=lambda amp: abs(amp[1])**2)[0] == w)
    return quantum_exec_time()


def phase_estimator(n):
    t = X(quant())
    c = H(quant(n))
    for i, j in enumerate(reversed(range(n))):
        ctrl(c[i], phase(2*pi*(pi/10)*2**j), t)
    adj(lib.qft, c)
    result = measure(c).value/2**n*10
    assert (abs(result-pi) < 0.1)
    return quantum_exec_time()


def rqc(n) -> float:
    seed(n)
    qubits = quant(n)
    gates = [RX(pi/2), RY(pi/2), T]
    pairs = list(range(n))
    shuffle(pairs)
    pairs = [(qubits[pairs[i]], qubits[pairs[i+1]]) for i in range(n-1)]
    sequence = {qubit: choice(range(3)) for qubit in qubits}
    for _ in range(n):
        shuffle(pairs)
        for c, t in pairs:
            for qubit in sequence:
                gates[sequence[qubit]](qubit)
                sequence[qubit] = (sequence[qubit]+choice([1, 2])) % 3
            cnot(c, t)
    measure(qubits)
    exec_quantum()
    return quantum_exec_time()


Ns = {N.bit_length(): N for N in list_N}


def shor(n: int) -> float:
    for _ in range(n):
        try:
            begin = time()
            _shor(Ns[n//2], _quantum_subroutine,
                  quantum=True, use_cached_x=True)
            end = time()
            break
        except:
            continue
    return end-begin


DEFAULT_SETUP = {
    "Hadamard": dict(func=hadamard, begin=3, end=25),
    "GHZ State": dict(func=ghz, begin=3, end=25),
    "W State": dict(func=w, begin=3, end=25),
    "Grover Algorithm": dict(func=grover, begin=3, end=15),
    "Phase Estimation Algorithm": dict(func=phase_estimator, begin=10, end=19),
    "Random Quantum Circuit": dict(func=rqc, begin=3, end=17)
}

FAST_SETUP = {
    "Hadamard": dict(func=hadamard, begin=3, end=16),
    "GHZ State": dict(func=ghz, begin=3, end=16),
    "W State": dict(func=w, begin=3, end=16),
    "Grover Algorithm": dict(func=grover, begin=3, end=11),
    "Phase Estimation Algorithm": dict(func=phase_estimator, begin=9, end=16),
    "Random Quantum Circuit": dict(func=rqc, begin=3, end=13)
}


def execute(name, simulator, save, setup=DEFAULT_SETUP, colab_download=False):
    simulator()
    result = {}
    for bmk in tqdm(setup, desc=name):
        func = setup[bmk]['func']
        if 'range' in setup[bmk]:
            result[bmk] = {n: func(n) for n in tqdm(
                setup[bmk]['range'], desc=bmk, leave=False)}
        else:
            begin = setup[bmk]['begin']
            end = setup[bmk]['end']
            result[bmk] = {n: func(n) for n in tqdm(
                range(begin, end), desc=bmk, leave=False)}
    with open(save, 'w') as file:
        json.dump({name: result}, file, indent=4)

    if colab_download:
        from google.colab import files
        files.download(save)


def plot():
    import plotly.express as px
    import os

    data = {}
    for file in os.listdir('data'):
        with open(os.path.join('data', file), 'r') as file:
            data = {**data, **json.load(file)}

    df = {}
    for sim in data:
        for bmk in data[sim]:
            if not bmk in df:
                df[bmk] = {}
                df[bmk]["N# Qubits"] = []
                df[bmk]["Time (s)"] = []
                df[bmk]["Simulator"] = []
            for n in data[sim][bmk]:
                df[bmk]["N# Qubits"].append(int(n))
                df[bmk]["Time (s)"].append(data[sim][bmk][n])
                df[bmk]["Simulator"].append(sim)
    for bmk in df:
        fig = px.line(
            df[bmk],
            title=bmk,
            x='N# Qubits',
            y='Time (s)',
            log_y=True,
            color='Simulator',
            width=2560/4,
            height=1440/4,
            markers=True
        )
        fig.update_yaxes(exponentformat='SI')

        fig.update_layout(
            plot_bgcolor='rgba(0,0,0,0)',
            paper_bgcolor='#f4f8ff',
            font_family='Manrope',
            font_color='#0f1f2e',
            font_size=10,
            xaxis=dict(tickmode='linear',
                       dtick=1,)
        )

        fig.write_image(f'plot/{"_".join(bmk.lower().split())}.svg')
